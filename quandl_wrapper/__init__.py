
from .extractor import get_closing_price
from .data.processor import load_company_list


COMPANY_DICT_LIST = load_company_list()