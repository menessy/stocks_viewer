from configuration.settings import BASE_DIR
"""
Data downloaded from
https://quandl-bulk-download.s3.amazonaws.com/WIKI-datasets-codes.zip?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAIHFXYT4EGZZUKNMQ%2F20171117%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20171117T192215Z&X-Amz-Expires=30&X-Amz-Security-Token=FQoDYXdzEJT%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaDEcvC47szulfPyQSfyKcA0afrWYajH3tR%2BU7lelRGO%2BojxZDyAiwGvBmsRrD9v8RejLTgUyd5SJrynZluygh1oFifNUuCtH%2F7%2FrlK4%2F2A0vWqjbzjPTBaZnIjsPkMxtO4FZ0K9VhsikAqHuR9yeNryeh2aPhClE45fcgy4Z46OPKmG7TmfzG2Y%2FjhjgQGLTUit%2BBvSTXSgd3FSzYfnr2ZcqQ9H%2FA%2FOshDE%2Fi2Kpr0GoMi8N43orCt4gqqVWu9t45jpL3Rg0IeK4O3JU0ZrPYL%2BVPF6cq%2BORHuotYGXIp%2FK3%2BTDLL89wam4k33%2Bs%2FFX3IaoEnjQ2k6MA6lGITYa7e4w7glbdvb2C%2BjGiOSNuqvfQM7fA3awgK77dV0QsOLqB1QnSbpiTxgiAL8YoIILGdMU8sWAB1YED7ME1G%2F32%2BHEegBeY0gJGRAnvG9tmwAwHHywI0xjjNBhrJF9AQr9QNkHstOJT0L807cVUaG%2FQZbSJIGUu5KwVjbLFHKAi%2BM4Ea993B4bGidf8ZU%2BysBQI77yjGm4S0MNK0%2F1301sLLGprt5QHQU%2BxFUhTDhjMom%2B280AU%3D&X-Amz-SignedHeaders=host&X-Amz-Signature=9c0fafb90a267a14cb4537350fc32d704a7a416c5d1a4eddf211e064ad2ae8cf

"""

FILE = 'original_data.txt'
OUTPUT_FILE = BASE_DIR + '/quandl_wrapper/data/data.txt'

def reformat_data(file):
    f = open(file, 'r')
    f2 = open(OUTPUT_FILE,'w')
    for line in f.readlines():
        _list = line.split(',')
        f2.write("%s,%s\n" % (_list[0].split('/')[1], _list[1][1:-7] ))
    f2.close()


def load_company_list():
    result = {}
    f = open(OUTPUT_FILE,'r')
    for l in f.readlines():
        code, company = l.split(',')
        result[code] = company[:-1]
    return result




if __name__ == "__main__":
    print(reformat_data(FILE))
