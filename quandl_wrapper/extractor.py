import quandl
from configuration.settings import QUANDL_API_SECRET_KEY

quandl.ApiConfig.api_key = QUANDL_API_SECRET_KEY




def get_closing_price(component, starting_date=None, ending_date=None):
    """
    :param component:
    :param starting_date:
    :param ending_date:
    :return: Pandas DataFrame Object
    """
    data = quandl.get_table('WIKI/PRICES',
                                qopts = { 'columns': ['date', 'close'] },
                                ticker = [ component ],
                                date = { 'gte': starting_date, 'lte': ending_date })

    return data.date.tolist() , data.close.tolist()





if __name__ == '__main__':
    get_closing_price('AAPL')
