from rest_framework import serializers





class StockSerializer(serializers.Serializer):
    component       = serializers.CharField(max_length=200, required=True)
    starting_time   = serializers.DateField(allow_null=True)
    ending_time     = serializers.DateField(allow_null=True)


    def get_starting_time(self):
        time = self.data['starting_time']
        return time

    def get_ending_time(self):
        time = self.data['ending_time']
        return time