from django.http import Http404
from django.http import HttpResponse
from django.template import loader
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import status


from api.serializers import StockSerializer
from api import constants
from quandl_wrapper import get_closing_price, COMPANY_DICT_LIST



class StocksView(APIView):
    """
    List all snippets, or create a new snippet.
    """
    def get(self, request, format=None):
        serializer = StockSerializer(data=request.GET)
        return Response(serializer.initial_data)

    def post(self, request, format=None):
        serializer = StockSerializer(data=request.data)
        if serializer.is_valid():
            _data = serializer.validated_data
            date_list, close_list = get_closing_price( _data[constants.COMPONENT], serializer.get_starting_time(),
                                   serializer.get_ending_time() )
            result = {
                'dates' : date_list,
                'close_prices': close_list
            }
            return Response(result, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class CompaniesListing(ListAPIView):


    def get(self, request, format=None):
        return Response(COMPANY_DICT_LIST)



def display_index_page(request):
    t = loader.get_template('index.html')
    context = {
        'companies_list' :  COMPANY_DICT_LIST
    }
    return HttpResponse( t.render(context, request) )

