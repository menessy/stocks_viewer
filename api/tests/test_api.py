from rest_framework.test import APITestCase
from django.core.urlresolvers import reverse
from rest_framework import status
from django.utils import timezone
import json

BASEURL = '/stocks_api/'
STOCKS_LISTING = BASEURL + 'get_companies_list'


class AccountTests(APITestCase):


    def test_component_extraction(self):
        data = {
                    'component': 'AAPL',
                    'starting_time' : '2016-11-11',
                    'ending_time' : '2017-11-11'
        }
        response = self.client.post(BASEURL, data, format='json')
        print(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, data)


    def test_get_companies_list(self):
        response = self.client.get(STOCKS_LISTING, format='json')
        print(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
